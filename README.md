# WIPER

[![pipeline status](https://gitlab.com/hyperd/wiper/badges/master/pipeline.svg)](https://gitlab.com/hyperd/wiper/-/commits/master)

Shell script to securely wipe specific files or folders, or cover your tracks on UNIX systems.
Designed for private people...

## Installation

`wiper.sh` works on UNIX based systems (e.g., Linux distros, macOS). Install it as follows:

```bash
wget --quiet -O /usr/local/bin/wiper "https://gitlab.com/hyperd/wiper/-/raw/master/wiper.sh";
chmod +x /usr/local/bin/wiper
# or
curl -o /usr/local/bin/wiper "https://gitlab.com/hyperd/wiper/-/raw/master/wiper.sh"
chmod +x /usr/local/bin/wiper
```

## Usage

`wiper [command] [flags] /path|file/to/wipe`

**Available Commands:**

- **wipe**              Wipe securely the content of a file or a folder, e.g., - `wiper wipe /your/path_or_file`
- **erase**             Erase the content of a file or a folder, e.g., - `wiper erase /your/path_or_file`
- **remove**            Remove the content of a file or a folder, e.g., - `wiper remove /your/path_or_file`
- **disable-logging**   Disable logging on the system, e.g., - `wiper disable-logging`
- **private**           Clear up your traces on the system, e.g., - `wiper private`
- **help**              Help about any command
- **version**           Print the version number of wiper

**Flags:**

- **-h**, **--help**        Help for `wiper`
- **-i**, **--iterations**  Number of iterations for the wipe command (default: 8); e.g., - `wiper wipe -i 64 /your/path_or_file`
- **-t**, **--timespan**    Timespan in minutes to consider a log file recently modified (default 120); e.g., - `wiper private -t 240`
- **-s**, **--silent**      Dry run, no questions asked; e.g., - `wiper wipe -s /your/path_or_file`
- **-r**, **--recursive**   Enable the main commands {wipe, erase, remove} to run recursive against any sub-directory; e.g., - `wiper wipe /your/path_or_file --recursive`

Use `wiper [command] --help` for more information about a command (not yet implemented).
